const bcrypt = require('bcryptjs');
const mongodb_name = "__"
module.exports = {
    port : 5000,
    sessionSecretKey : bcrypt.hashSync("SECRET_KEY", 2),
    MONGODB_DATABASE_NAME : mongodb_name,
    MONGO_URL  : `mongodb://localhost/${mongodb_name}`,
    lastfm : {
        "APIKEY" : '18c5925e73c6f99a398803bc3f6a7c59',
        "Application name" :	"trail",
        "Registered to"	:"AnikethSaha"
      },
    LASTFM_SECRET : '30654093483d7d8a299f15ee1cc6fef5',
}
