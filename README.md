# TrueCaller InternShip Task

## Music Searching App using `lastfm` API

## Build with MERN Boilerplate By [Aniketh Saha](https://github.com/anikethsaha/MERN-Boilerplate)

> Hosting is still Pending


## Getting Started
- ##### Clone the project
- ##### Install the Dependencies
```bash
$ npm i
```
- ##### Run the Webpack
```bash
$ npm run build
```
- ##### Run the express server
```bash
$ npm run server:dev
```

- ##### Go To localhost:5000


> I have removed the Mongodb Connection method as I am not using any kind of storage in this project


## Production script




```bash
$ npm run start

  ----or----


$ npm run server:prod
```


##  Changes in development mode and Production mode

### Lastfm API

1. Change in src/client/redux/initialState.js

For `development mode` :
```bash
const initialState = {
  lastfm : {
    "APIKEY" : '18c5925e73c6f99a398803bc3f6a7c59',
    "Application name" :	"MERN Stack DEMO",
    "Registered to"	:"AnikethSaha"
  },
  searchHistory : [],
  artist : {
    info : {},
    topSongs : [],
    Similar : {}
  }
};



module.exports = {
    initialState
}



```


For `production mode` :
```bash
const initialState = {
  lastfm : {
    "APIKEY" : '4d05ca8b04d4ce4d098e4f5d27cd7500',
    "Application name" :	"MERN Stack DEMO",
    "Registered to"	:"AnikethSaha"
  },
  searchHistory : [],
  artist : {
    info : {},
    topSongs : [],
    Similar : {}
  }
};



module.exports = {
    initialState
}



```







## Look for the `config/index.js` for making changes in the configs of the project
- Edit the src/client/index.js to make changes for client

- Edit the src/components to make changes in components

- Edit the src/server/index.js to make changes in the server


## How To use the website

- Go To `http://localhost:5000/#/`
- A search Field Will come
- Enter the Artist name
- Wait for the response from `lastfm` API
- if some error it will show message regarding that
- If all seems correct then will result with three things
  - Top Songs : It will come in the same page
  - Similar Artist : Click on the Similar `Artist Icon` or go to `http://localhost:5000/#/similar`
  - Info About the Artist : Click on the `Info Icon` or go to `http://localhost:5000/#/info`
- The successful searches will add in the search history list in the home page



## technology
- NodeJS - Server
- ExpressJS - Nodejs framework
- MongoDB - Database - Not in use for this project example
- ReactJS - Frontend
- Redux - State Management
- Docker - Containerization and image
- Eslint - Linter
- Webpack - Bundler
- Babel - Loader and Compiler/Transpiler
- Jest - Testing Framework


## Features
-  Server Side Rendering
-  State Management
-  Image for Container
-  Routing
-  Model Controller Project Structure



## [LICENSE](https://github.com/anikethsaha/MERN-BoilerPlate/blob/master/LICENSE)

