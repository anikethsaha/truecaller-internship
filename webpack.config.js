const path = require('path');
var webpack = require('webpack');
// const nodeExternals = require('webpack-node-externals');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const devMode = process.env.NODE_ENV !== 'production';
const BrowserConfig = {
  name: 'browser',
  entry: './src/client/index.js',
  output: {
    path:  path.resolve(__dirname, "./public"),
    filename: './js/app.bundle.js',
    publicPath: './'

  },
  target: 'web',
  module: {
    rules: [{
        test: /\.js?$/,
        exclude: /node_modules/,
        use: 'babel-loader'

      },
      {
        test: /\.css$/,
        use: [{
            loader: MiniCssExtractPlugin.loader,
            options: {
              // you can specify a publicPath here
              // by default it uses publicPath in webpackOptions.output
              publicPath: './css/',
              hmr: process.env.NODE_ENV === 'development',
            },
          },
          {
            loader: 'css-loader',
          }

        ],

        include: /node_modules/
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: 'babel-loader'

      }
    ]

  },
  devServer: {
    historyApiFallback: true,
  },
  stats: {
    colors: true
  },
  optimization: {
    splitChunks: {
      chunks: 'async',
      minSize: 30000,
      maxSize: 0,
      minChunks: 1,
      maxAsyncRequests: 5,
      maxInitialRequests: 3,
      automaticNameDelimiter: '~',
      name: true,
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true
        }
      }
    }
  },
  devtool: 'source-map',
  plugins: [
    new webpack.DefinePlugin({
      __isBrowser__: "true"
    }),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
       filename: devMode ? '[name].css' : '[name].[hash].css',
       chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
    }),
  ]
};

const ServerConfig = {
  name: 'server',
  entry: './src/server/index.js',
  output: {
    path:  path.resolve(__dirname, "./bin"),
    filename: './server.js',
    libraryTarget: "commonjs2",
    publicPath: './'
  },
  node: {
    __dirname: false
  },
  target: 'node',
  // externals: [nodeExternals()],
  plugins: [
    new webpack.DefinePlugin({
      __isBrowser__: "false"
    }),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
           chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
           filename: devMode ? '[name].css' : '[name].[hash].css',
    }),
  ],
  module: {
    rules: [{
        test: /\.js?$/,
        exclude: /node_modules/,
        use: 'babel-loader'

      },
      {
        test: /\.css$/,
        use: [{
            loader: MiniCssExtractPlugin.loader,
            options: {
              // you can specify a publicPath here
              // by default it uses publicPath in webpackOptions.output
              publicPath: './css/',
              hmr: process.env.NODE_ENV === 'development',
            },
          },
          {
            loader: 'css-loader',
          }

        ],

        include: /node_modules/
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: 'babel-loader'

      }
    ]
  },
  optimization: {
    splitChunks: {
      chunks: 'async',
      minSize: 30000,
      maxSize: 0,
      minChunks: 1,
      maxAsyncRequests: 5,
      maxInitialRequests: 3,
      automaticNameDelimiter: '~',
      name: true,
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true
        }
      }
    }
  },
  devServer: {
    historyApiFallback: true,
  }
};

module.exports = [BrowserConfig, ServerConfig];
// module.exports =[ BrowserConfig ];
