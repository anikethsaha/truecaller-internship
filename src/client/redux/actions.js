module.exports = {
    sampleAction : (data) => {
        return ({
            type : "",
            data
        })
    },
    addLastFMData : data => ({
      type : "ADD_LAST_FM_DATA",
      data
    }),
    addArtistInfo : data => ({
      type : "ADD_SEARCHED_ARTIST_DATA",
      data
    }),
    addHistory : data => ({
      type : "ADD_SEARCH_HISTORY",
      data
    })

}
