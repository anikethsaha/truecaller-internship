import { initialState } from './initialState'
import { resolve } from 'path'; // eslint-disable-line
import _ from 'lodash'; // eslint-disable-line


export const  reducer =  (state = initialState,Action) => {
    switch (Action.type) {
        case "ADD_LAST_FM_DATA" :
        var newState = _.assignIn({},state,{
              lastfm : {
                    ...Action.data
                  }
            })

          return  newState;
          break;
        case "ADD_SEARCHED_ARTIST_DATA" :


            return {
              ...state,
              ...Action.data
            };
            break;
        case "ADD_SEARCH_HISTORY" :
            state.searchHistory.push(Action.data)
            if(localStorage.getItem("searchHistory") == null)
            {

              localStorage.setItem("searchHistory",Action.data);

            }else{
               let localStorageData = localStorage.getItem("searchHistory")
              localStorageData = localStorageData + `,${Action.data}`;
              localStorage.setItem("searchHistory",localStorageData)
            }
            return state;
            break;
        default:
            return  state;
    }
}









