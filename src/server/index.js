const express = require('express')
const bodyParser = require('body-parser')
var expressControllers = require('express-controller');
var session = require('express-session')
const cors = require('cors')
const helmet = require('helmet')
const { port , sessionSecretKey} = require('../../config')
const path  = require('path')
var csrf = require('csurf');

import process from 'process'

// var RateLimit = require('express-rate-limit')


// SSR method importing from ./controller/ssr
import { renderSSRcomponent } from "./controller/ssr";









// M
// Middlewares
const app = express();
app.use(helmet());

// session Middleware
app.use(session({
    secret: sessionSecretKey,
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true }
  }))
app.use(csrf());
app.set('port', (process.env.PORT ||port));

app.use(bodyParser.urlencoded({extended: true, limit: '50mb'}))
app.use(bodyParser.json({limit: '50mb'}))
app.use(cors());
// use this middleware in authentications routes or post method routes
/* var authAPILimiter = new RateLimit({
    windowMs: 5*60*1000, // 5 minutes
    max: 1000,
    delayMs: 0 // disabled
  });
*/


// V
// static files and views
app.use(express.static(path.join(__dirname, '../public')))
app.set('view engine' , 'ejs');
app.engine('html', require('ejs').renderFile);
app.set('views' , path.join(__dirname,'views'));


// C
//controller settings
//setting up the controller
expressControllers.setDirectory(path.join(__dirname,'/controller')).bind(app);


// routes

app.get('/',renderSSRcomponent)

app.listen(app.get('port'),() => {
console.log(path.join(__dirname, '../public'))
    console.log( `> Server is running on PORT ${app.get('port')} `);
    console.log('Node ENV :',  process.env.NODE_ENV);
})
