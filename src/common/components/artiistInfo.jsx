import React from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import {
  Typography,

  Carousel,
  Card
} from 'antd';
const {Title} = Typography;
import {  Row, Col } from 'react-flexbox-grid';
class ArtistInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.addImageToSliderHandler = this.addImageToSliderHandler.bind(this);
  }
  addImageToSliderHandler(){
    return this.props.info.image.map(item => {
      return (
        <div> <img src={Object.values(item)[0]} alt="Image" /> </div>
      )
    })
  }

  render() {
    const cardCustomStyle ={
      margin : ".5rem",
      backgroundColor : "rgb(236, 236, 236)",
      padding : "5px"
    }
    const fullWidth = {
      width :"100%"
    }
    const pContentStyle = {
      fontWeight : "700",
      letterSpacing : ".1rem",
    }
    const pStyle = {
      textAlign : "right",
      fontSize : "1.5rem"
    }
    return (
      <div>

          <Row center="xs">
            <Col xs={10}>
              <Row center="xs">
              {
                    _.isEmpty(this.props.info)?
                        <Title level={2}>
                            <img src="https://img.icons8.com/flat_round/28/000000/no-entry.png" />
                            &nbsp; No Artist To Show
                        </Title>
                      :
                      <div id="artist_top_songs" style={fullWidth}>
                        <h2>  About {this.props.info.name} </h2>
                        <br/>
                        <Row center="xs">
                          <Col xs={12}>
                          <Card
                            bordered={true}
                            style={{...cardCustomStyle,...fullWidth}}
                          >
                          <Row>
                            <Col xs={6}>
                              <p
                              style={{...pContentStyle ,...pStyle}}
                              >{this.props.info.bio.content} </p>
                              <br/>
                              <span>{this.props.info.bio.published}</span>
                              <br/>
                              <a
                              {...this.props.info.bio.links}>Visit {this.props.info.name}</a>
                              <br/>
                              <p> Listeners : {this.props.info.stats.listeners}   </p>
                              <p> Playcount : {this.props.info.stats.playcount}   </p>


                            </Col>
                            <Col xs={6}>
                            <Carousel autoplay>
                              {this.addImageToSliderHandler()}
                            </Carousel>
                            </Col>
                          </Row>

                          </Card>

                          </Col>
                        </Row>
                      </div>
                }
              </Row>
            </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => state.artist;

export default connect(
  mapStateToProps,
  null
)(ArtistInfo);
