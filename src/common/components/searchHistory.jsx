import React from 'react';
import {connect} from 'react-redux'
import {
  Skeleton,
  List,
  Tag
} from 'antd'
class SearchHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isHistoryLoading : true,
      searchHistoryArray : [],
      isLocalStorageSearchHistorySet : false
    };
  }

  componentDidMount(){
    const isLocalStorageSearchHistorySet = localStorage.getItem("searchHistory") ? true : false;
    const localStorageData = localStorage.getItem("searchHistory") || "";
    this.setState({
      searchHistoryArray : localStorageData.split(',') || [] ,
      isHistoryLoading : false,
      isLocalStorageSearchHistorySet
    })
  }
  renderHistoryListHandler(){
    // return this.state.searchHistoryArray.map(search => {
    //   return (

    //   );
    // })
  }
  render() {
    return (
      <div>
        { this.state.isHistoryLoading? <Skeleton active /> :
        <div>

            { !this.state.isLocalStorageSearchHistorySet?
              <h4>No Search History Found </h4>
              :
              <List
                size="small"
                header={
                <h3>
                  <img src="https://img.icons8.com/cotton/34/000000/clock-checked.png" />
                  &nbsp;  Search History
                </h3>
               }
                bordered={false}
                dataSource={ this.state.searchHistoryArray }
                renderItem={
                  item => (
                  <List.Item style={{border:"none"}}>
                    <Tag style={{
                      width : "100%"
                    }}>
                       {item}
                    </Tag>
                  </List.Item>
                )}
              />
            }

        </div>

        }


      </div>
    );
  }
}

const mapStateToProps = state => state.searchHistory  ;

export default connect(
  mapStateToProps,
  null
)(SearchHistory);
