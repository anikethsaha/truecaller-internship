import React from 'react';
import { PageHeader } from 'antd';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
         <PageHeader

            title="Truecaller"
            subTitle="Internship Assignment"
          />
      </div>
    );
  }
}



export default Header;
