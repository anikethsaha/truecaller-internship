import React from 'react';
import {Provider} from 'react-redux'
import { store } from "../../client/redux";
import "antd/dist/antd.css";
import {
  Switch,
  HashRouter ,
  Route,
} from 'react-router-dom'
import SimilarArtist from './similarArtist.jsx'
import Header from './header.jsx'
import ArtistInfo from './artiistInfo.jsx'
import Main from './main.jsx';
class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
      const MidStyle = {
        marginTop : "5rem"
      }
        return (

              <Provider store = {store}>
                <div >
                <Header/>
                     <HashRouter>
                      <Switch>

                        <div className="container" style={MidStyle} >
                          <Route exact path="/" component={Main} />
                          <Route exact path="/similar" component={SimilarArtist } />
                          <Route exact path="/info" component={ArtistInfo } />
                        </div>
                      </Switch>
                    </HashRouter>
                </div>
               </Provider>

        );
    }
}

App.propTypes = {};

export default App;
