import React from 'react';
import {  Row, Col } from 'react-flexbox-grid';
import { Input } from 'antd';
import { connect } from 'react-redux';
import axios from "axios"
import {
  addArtistInfo,
  addHistory
} from '../../client/redux/actions'
import SearchHistory from './searchHistory.jsx'
import {
  notification,
  message,

  Tooltip ,

  Skeleton
 } from 'antd';
 import TopSongsResult  from './topSongsResult.jsx'
class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      API_REQUEST_LOADING : false,
      RESULT_DISPLAY : false,
      artist : {
        info : {},
        topSongs : [],
        Similar : {}
      }
    };
    this.notificationHandler = this.notificationHandler.bind(this);
    this.SearchHandler = this.SearchHandler.bind(this);
  }

  componentDidMount(){
  }
  notificationHandler(msg,type){
    notification.open({
      message: type,
      description: `${msg}`,
    });
    // message.error(`${msg}`);
  }
  SearchHandler(value){
    this.setState({
      API_REQUEST_LOADING : true
    })
    message.info(`Getting Details For ${value} `);
    axios.get(
      `https://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=${value}&api_key=${this.props.lastfm.APIKEY}&format=json`)
      .then(res => {
        message.info(`Getting Top Tracks of ${value} `);

        const {data } = res;
        if(data.error > 0 || data.message == "The artist you supplied could not be found"){
          message.error(data.message);
          this.setState({
            API_REQUEST_LOADING : false,
            RESULT_DISPLAY : false,
          });
          return ;
        }


        axios.get(`https://ws.audioscrobbler.com/2.0/?method=artist.gettoptracks&artist=${value}&api_key=${this.props.lastfm.APIKEY}&format=json`)
        .then(res2 => {

          const {artist} = res.data;
          let dataToSendToStore = {
            artist : {
              info : {
                bio : artist.bio,
                image : artist.image,
                mbid : artist.mbid,
                name : artist.name,
                ontour : artist.ontour,
                stats : {...artist.stats},
                tags : {...artist.tag},
                url : artist.url
              },
              similar : {...artist.similar},
              topSongs : res2.data.toptracks.track
            }
          }
          message.info(`Please Wait `);
          this.props.addArtistInfo(dataToSendToStore);
          message.success(`Done`);
          this.props.addHistory(value);
          this.setState({
            API_REQUEST_LOADING : false,
            RESULT_DISPLAY : true
          })

        })
        .catch(err =>  {
          this.notificationHandler(err,"NETWORK ERROR")
          this.setState({
            API_REQUEST_LOADING : false,
            RESULT_DISPLAY : false,
          });
        })

      } )
      .catch(err2 => {
        this.notificationHandler(err2,"NETWORK ERROR")
        this.setState({
          API_REQUEST_LOADING : false,
          RESULT_DISPLAY : false,
        });
      })

  }

  render() {
    const Search = Input.Search;
    // const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />
    return (
      <div >
        <Row>
        <Col xs={12}>
          <Row center="xs">
            <Col xs={6} >
            <Search
              placeholder="Enter Artist Name"
              onSearch={this.SearchHandler}
              style={{ width: 400 }}
            />

            </Col>
          </Row>
          <div className="m-2">
            <Row center="xs"  >
                <Col xs={1} >
                <Tooltip title="Top Songs">
                  <a href="/">
                    <img src="https://img.icons8.com/dotty/38/000000/micro.png" />
                  </a>

                </Tooltip>

                </Col>
                <Col xs={1} >
                <Tooltip title="Info">
                  <a href="#info" >
                    <img src="https://img.icons8.com/dotty/38/000000/info.png" />
                  </a>
                </Tooltip>

                </Col>
                <Col xs={1} >
                <Tooltip title="Similar Songs">
                  <a href="#similar" >
                    <img src="https://img.icons8.com/ios/38/000000/similar-items-filled.png" />
                  </a>
                </Tooltip>
                </Col>
              </Row>
          </div>

        <div className="m-2">
          <Row center="xs">
              <Col xs={10} >
                { this.state.API_REQUEST_LOADING ? <Skeleton active />: <span></span> }
              </Col>
            </Row>
        </div>

        <div className="m-2">
          <Row center="xs">
              <Col xs={12} >
                  { this.state.RESULT_DISPLAY ? <TopSongsResult /> : <div></div> }
              </Col>
            </Row>
        </div>

        <div className="m-2">
          <Row center="xs">
              <Col xs={8}>
                  <SearchHistory />
              </Col>
          </Row>
        </div>

        </Col>
        </Row>

      </div>
    );
  }
}


const mapStateToProps = state => {
  return {
    lastfm : state.lastfm
  }
} ;



export default connect(
  mapStateToProps,
  {addArtistInfo,addHistory}
)(Main);
