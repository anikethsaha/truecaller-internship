import React from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import {
  Typography,
  List,
  Card
} from 'antd';
const {Title} = Typography;
import {  Row, Col } from 'react-flexbox-grid';
class SimilarArtist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const cardCustomStyle ={
      margin : ".5rem",
      backgroundColor : "rgb(236, 236, 236)",
      padding : "5px"
    }
    const fullWidth = {
      width :"100%"
    }
    return (
      <div>

          <Row center="xs">
            <Col xs={10}>
              <Row center="xs">
              {
                    _.isEmpty(this.props.similar)?
                        <Title level={2}>
                            <img src="https://img.icons8.com/flat_round/28/000000/no-entry.png" />
                            No TopSongs To Show
                        </Title>
                      :
                      <div id="artist_top_songs" style={fullWidth}>
                        <h2> {_.size(this.props.similar.artist)} &nbsp; Top Songs Found </h2>
                        <br/>
                        <Row center="xs">
                          <Col xs={12}>
                          <List

                              itemLayout="horizontal"
                              dataSource={this.props.similar.artist || []}
                              renderItem={item => (
                                <List.Item
                                  style={{
                                    listStyle : "none",
                                    width : "100%",
                                    borderBottom : "none"
                                  }}
                                >
                                  <Card

                                    bordered={true}
                                    style={{...cardCustomStyle,...fullWidth}}
                                  >

                                    <h5> <a href={item.url}><img src="https://img.icons8.com/ultraviolet/40/000000/circled-play.png" /></a> {item.name}</h5>
                                  </Card>
                                </List.Item>
                              )}
                            />
                          </Col>
                        </Row>
                      </div>
                }
              </Row>
            </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => state.artist;

export default connect(
  mapStateToProps,
  null
)(SimilarArtist);
