import React from 'react';
import {connect} from "react-redux"
import {
  Typography ,
} from 'antd';

import {  Row,  } from 'react-flexbox-grid';
import SongCard from './songCard.jsx'
const { Title } = Typography;
import _ from 'lodash'
class TopSongsResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.topSongsListHandler = this.topSongsListHandler.bind(this)
  }

  topSongsListHandler(){
    return  this.props.topSongs.map(song => {
      return (
        <SongCard song={song} />
      );
    });

  }



  render() {
    return (
      <div className="m-2">

      {
        _.isEmpty(this.props.topSongs)?
            <Title level={2}>
                <img src="https://img.icons8.com/flat_round/28/000000/no-entry.png" />
                &nbsp;  No TopSongs To Show
            </Title>
          :
          <div id="artist_top_songs">
            <h2> {_.size(this.props.topSongs)} &nbsp; Top Songs Found </h2>
            <br/>
            <Row>
              {this.topSongsListHandler()}
            </Row>
          </div>
    }


      </div>
    );
  }
}
const mapStateToProps = state => state.artist;

export default connect(
  mapStateToProps,
  null
)(TopSongsResult);
