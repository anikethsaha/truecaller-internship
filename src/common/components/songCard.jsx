import React from 'react';
import {
  Icon,
  Card
} from 'antd';
import {   Col } from 'react-flexbox-grid';
class SongCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const spanPlayCountStyle = {
      height : "1rem",
      width : "100%"
    }
    const cardCustomStyle = {
      // height : "17rem",
      margin : ".5rem",
      backgroundColor : "rgb(236, 236, 236)",
      padding : "5px"
    }
    const song= this.props.song
    return (
      <Col xs={3} >
      <Card
        bordered={true}
        style={cardCustomStyle}
        >
        <span style={spanPlayCountStyle}> <Icon type="customer-service" theme="filled" /> {song.playcount} </span>
        <h5  > <img src="https://img.icons8.com/android/24/000000/musical-notes.png" />  {song.name}</h5>
        <span style={spanPlayCountStyle}><img src="https://img.icons8.com/color/24/000000/conference-foreground-selected.png" /> {song.listeners} </span>
        <br/>
        <a href={song.url} style={{...spanPlayCountStyle,color : "#f30e0e"}}><img src="https://img.icons8.com/ultraviolet/30/000000/circled-play.png" /></a>
      </Card>
    </Col>
    );
  }
}


export default SongCard;
